module zeroExtender#(parameter WIDTH = 16)
(
	input [WIDTH-1:0] sixteen_bit_in,
	
	output reg [(WIDTH*2)-1:0] thirtytwo_bit_out
);

always @ (*)
begin
		thirtytwo_bit_out = {16'b0, sixteen_bit_in};
end

endmodule