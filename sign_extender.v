module SignExtender#(parameter WIDTH = 16)
(
	input [WIDTH-1:0] sixteen_bit_in,
	output reg [(WIDTH*2)-1:0] thirtytwo_bit_out
);

always @ (*)
	begin
		if(~sixteen_bit_in[WIDTH-1])
		begin
			thirtytwo_bit_out = { 16'h0000, sixteen_bit_in};
		end
		else
		begin
			thirtytwo_bit_out = { 16'hFFFF, sixteen_bit_in};
		end	
	end
endmodule
