module control_unit#(parameter W = 32)(
	input[5:0] opcode_in,
	input[5:0] funct_in,
	input branch_LSB_in,
	
	//--- IF
	//--- ID
	//--- MEM
	//--- EXE
	output reg unsigned_trigger_out,
	output reg logical_extension,
	output reg [1:0] pc_jumper, //--- input for mux4 into the PC
	output reg [1:0] size_out,
	output reg [5:0] funct_in_out, 
	output reg //reg_dst_out, 
				  alu_src_out, 
				  reg_write_out,
				  re_in_out, 
				  we_in_out, 
//				  mem_to_reg_out,
				  jump_out,
	output reg [1:0] reg_dst_out,
	output reg [1:0] mem_to_reg_out
);
	reg [17:0] cmd_bits;
	reg [40:0] cmds;
	reg [2:0] extra_bits; //--- extra_bits[1] for jump-link ops,
								 //--- extra_bits[0] for LUI
	//--- Operation decomposition
	//--- [UTrig][ Imm ][ JMP Mux][ DModule Size][ALU Operation Bits][Reg Dest][ALUSrc][RegWrite][MemRead][MemWrite][MemToReg][Jump]
	//---        [ X   ][ XX     ][ XX          ][      XXXXXX      ][   X    ][   X  ][   X    ][   X   ][   X    ][    X   ][  X ]
	//--- Exa_bit loc
   //--- [  2  ]                                                    [   1    ]                                     [    0   ]
	// 0_0001_1110_1000_0000
	// 01E80
	always @ (*) begin
		cmd_bits = 0;
		extra_bits = 0;
		cmds = "";
		case(opcode_in) // follows table in the slides for Lab 3
			//--- I-TYPE Operations
			//--- --- Load operations
			6'h20: begin 
						 cmd_bits = 18'h0103A; //--- Load byte 0-00-00-100000-0-1-1-1-0-1-0
						 cmds = "LB";                 
					 end
			6'h21: begin
			         cmd_bits = 18'h0303A; //--- Load Half-word
						cmds = "LH";
				    end
			6'h23: begin 
			         cmd_bits = 18'h0703A; //--- Load word
						cmds = "LW";
					 end
			6'h24: begin
			         cmd_bits = 18'h0103A; //--- Load byte unsigned
						extra_bits[2] = 1; //--- activate unsigned trigger
						cmds = "LBU";
					 end
			6'h25: begin
			         cmd_bits = 18'h0303A; //--- Load Half-word unsigned
						extra_bits[2] = 1; //--- activate unsigned trigger
						cmds = "LHU";
					 end
			6'h0F: begin
			           cmd_bits = 18'h01010;//--- Load upper immediate
						  cmds = "LUI";
						  extra_bits[0] = 1;
			           extra_bits[1] = 0;
				    end
			//--- --- Store Operations
			6'h2B: begin
			         cmd_bits = 18'h07024; //--- store word
						cmds = "SW";
					 end
			6'h28: begin
			         cmd_bits = 18'h01024; //--- Store byte 00_0001
						cmds = "SB";
					 end
			6'h29: begin
			        cmd_bits = 18'h03024; //--- Store Half-word
					  cmds = "SH";
					 end
			//--- --- Arithmetic I-Types
			6'h08: begin
			         cmd_bits = 18'h01030; //--- ADDI
						cmds = "ADDI";
					 end
			6'h09: begin
			        cmd_bits = 18'h010B0; //--- ADDIU
					  cmds = "ADDIU";
					 end
			//--- --- Logicial I-Types
			6'h0C: begin
			        cmd_bits = 18'h21230; //--- ANDI
					  cmds = "ANDI";
					 end
			6'h0D: begin 
		           cmd_bits = 18'h212B0; //--- ORI
					  cmds = "ORI";
					 end
			6'h0E: begin
			        cmd_bits = 18'h21330; //--- XORI
					  cmds = "XORI";
					 end
			//--- --- Branching Operations:
			6'h01: //--- Branch path for BLTZ and BGEZ
				case(branch_LSB_in)
					0: begin 
					     cmd_bits = 18'h01C20; //--- BLTZ
						  cmds = "BLTZ";
						 end
					1: begin
					    cmd_bits = 18'h01C80; //--- BGEZ
						 cmds = "BGEZ";
						end
				endcase
			6'h04: begin
			        cmd_bits = 18'h01E00; //--- BEQ
					  cmds = "BEQ";
					 end
			6'h05: begin
			        cmd_bits = 18'h01E80; //--- BNE
					  cmds = "BNE";
					 end
			6'h06: begin
			        cmd_bits = 18'h01F00; //--- BLEZ
					  cmds = "BLEZ";
					 end
			6'h07: begin
			        cmd_bits = 18'h01F80; //--- BGTZ
					  cmds = "BGTZ";
					 end
			//--- J-TYPE Operations
			6'h02: begin
			        cmd_bits = 18'h01D01; //--- J
					  cmds = "J";
					 end
			6'h03: begin 
			       cmd_bits = 18'h09D13;//--- JAL
					 cmds = "JAL";
					 extra_bits[1] = 1;
					 extra_bits[0] = 1;
					 end
			//--- R-TYPE Operations
			0: //--- Code required for the R-Type instructions
				case(funct_in)
				6'h08: begin
				        cmd_bits = 18'h11D81; //--- JR
						  cmds = "JR";
						 end
   			6'h09: begin 
				        cmd_bits = 18'h19D13; //--- JALR
						  cmds = "JALR";
 						  extra_bits[0] = 1;
						 end
				6'h20: begin
				        cmd_bits = 18'h01050; //--- ADD
						  cmds = "ADD";
						 end
				6'h21: begin
				        cmd_bits = 18'h010D0; //--- ADDU
						  cmds = "ADDU";
						 end
				6'h22: begin
				        cmd_bits = 18'h01150; //--- SUB
						  cmds = "SUB";
						 end
				6'h23: begin
				        cmd_bits = 18'h011D0; //--- SUBU
						  cmds = "SUBU";
						 end
				6'h24: begin
				        cmd_bits = 18'h01250; //--- logical AND
						  cmds = "AND";
						 end
				6'h25: begin
				        cmd_bits = 18'h012D0; //--- logical OR
						  cmds = "OR";
						 end
				6'h26: begin 
				        cmd_bits = 18'h01350; //--- XOR
						  cmds = "XOR";
						 end
				6'h27: begin
				        cmd_bits = 18'h013D0; //--- NOR
						  cmds = "NOR";
						 end
				6'h2A: begin
				        cmd_bits = 18'h01450; //--- SLT
						  cmds = "SLT";
						 end
				6'h2B: begin
				        cmd_bits = 18'h014D0; //--- SLTU
						  cmds = "SLTU";
						 end
				6'h00: begin
				        cmd_bits = 0        ; //--- NOP
						  cmds = "NOP";
						 end
				default: cmd_bits = 18'hXXXXX; 
				endcase
			default: cmd_bits = 18'hXXXXX;
		endcase
		
		unsigned_trigger_out = extra_bits[2];
		logical_extension = cmd_bits[17];
		pc_jumper = cmd_bits[16:15]; //--- J = 00, JAL = 01, JR = 10, JALR = 11
		size_out = cmd_bits[14:13];  //---  
		funct_in_out = cmd_bits[12:7];
		reg_dst_out = {extra_bits[1], cmd_bits[6]};
		alu_src_out = cmd_bits[5]; 
		reg_write_out = cmd_bits[4];
		re_in_out = cmd_bits[3];
		we_in_out = cmd_bits[2]; 
		mem_to_reg_out = {extra_bits[0], cmd_bits[1]};
		jump_out = cmd_bits[0];
//		$display("%s", cmds);
	end	
endmodule
