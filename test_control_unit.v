`timescale 1ns/1ps

module test_control_unit#(parameter W = 6);
	reg clk;
	reg [W-1:0] opcode_in;
	reg [W-1:0] func_in;
	wire [5:0] funct_in_out;
	wire reg_dst_out, 
				  alu_src_out, 
				  reg_write_out,
				  re_in_out, 
				  we_in_out, 
				  mem_to_reg_out,
				  branch_out;
	
	control_unit dut (.opcode_in(opcode_in), .funct_in(func_in), .funct_in_out(funct_in_out), .reg_dst_out(reg_dst_out), .alu_src_out(alu_src_out), .reg_write_out(reg_write_out), .re_in_out(re_in_out), .we_in_out(we_in_out), .mem_to_reg_out(mem_to_reg_out), .branch_out(branch_out));
	
	initial begin
		clk = 0;
		forever #1 clk = ~clk;
	end
	reg [12:0] cmd_bits_out;
	
	initial begin
		func_in = 0;
		opcode_in = 0;
		// initialized the opcode and func code inputs
		@(posedge clk);
		opcode_in = 6'h23;
		
		@(posedge clk);
		opcode_in = 0;
		func_in = 6'h20;
		
		@(posedge clk);
		opcode_in = 6'h2b;
		@(posedge clk);
		opcode_in = 6'h23;
		@(posedge clk);
		opcode_in = 6'h08;
		@(posedge clk);
		opcode_in = 6'h01;
		
	end
endmodule
