`timescale 1ns/1ps

module test_signextender;
	reg clk;
	
	initial begin
		clk = 0;
		forever #10 clk = !clk;
	end
	
	reg [15:0] sixteen_bit_in = 16'hFFFF;
	
	SignExtender dut(.sixteen_bit_in(sixteen_bit_in));
	
	initial begin
	
	end
	
endmodule
