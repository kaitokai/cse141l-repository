module processor#(parameter W = 32)(
	input clock,
	input reset,
	
	input [7:0] serial_in,
	input serial_valid_in,
	input serial_ready_in,
	
	output [7:0] serial_out,
	output serial_rden_out,
	output serial_wren_out
);
localparam ADDER_CONST = 32'h0000_0004;
localparam PATH = "C:/Users/KLE/Documents/cse141l-repository/memh/nbhelloworld/";
localparam LEAD_NAME = "nbhelloworld";
localparam INSTR_NAME = {LEAD_NAME, ".inst_rom.memh"};
localparam RAM_NAME = {LEAD_NAME, ".data_ram"};

//--- Control unit inputs and outputs
wire [W-1:0] pc_output;
wire [W-1:0] adder_output;
wire [1:0] RegDst;
wire Branch, re_in;
wire [1:0] MemToReg;
wire we_in, ALUSrc, RegWrite;
wire [5:0] Funct_in;
wire [1:0] size_out;
wire [1:0] pc_jumper;
wire unsigned_trigger_out;

//--- Sign extension
wire [W-1:0] sign_extnd_imm;
//--- Zero'd immediate
wire [W-1:0] zeroed_imm;

wire [W-1:0] instruction_out;

wire [4:0] write_reg_from_mux;

//--- Register wires
wire [W-1:0] read_data_one; // data that will be fed into the A_in pin ALU
wire [W-1:0] read_data_two; // data that will be fed into the myDataIntoALUMux
wire [W-1:0] to_write_data; // potentially the location to write data, i.e. dest

//---
wire [W-1:0] toJumpMuxB; //--- wire to MUX to determine the type of jump or branch addr
wire [W-1:0] pcJorB; //--- PC Address for Jump or Branch


wire Jump;
assign Jump = Branch; 	//--- Jump -> Branch, so we don't need to change any legacy code

wire Jump_out; 			//--- wire for the ALU Jump out condition
wire Branch_out; 			//--- wire for the ALU Branching condition

wire pc_mux_cond;			//--- wire for MUX into PC determining whether we are jumping or branching
assign pc_mux_cond = Branch_out | Jump_out; //--- OR'ing to verify if we are branched or jumping
wire [W-1:0] toPCInput;
/**** BEGIN INSTRUCTION FETCH PHASE */
wire [W-1:0] if_id_inst;
wire [W-1:0] if_id_pc;
wire if_id_w_e;
wire if_id_pc_w_e;
	//--- MUX determines whether we are doing typical PC+4, J-Ops, or B-Ops
	Mux2 #(.W(W)) myPCMux (.A_in(adder_output), 
								  .B_in(pcJorB), 
								  .sel_in(pc_mux_cond), 
								  
								  .V_out(toPCInput));
	//--- Program Counter initialization
	PC myPC(.clock(clock), 
			  .reset(reset), 
			  .pc_w_e(1),
			  
	//		  .locat_in(adder_output), 
			  .locat_in(toPCInput),
			  
			  .addr_out(pc_output));
			  
	//--- Adder to drive the Program Counter
	Adder #(.WIDTH(W)) myPCAdder(.a_in(ADDER_CONST), 
										  .b_in(pc_output), 
										  
										  .sum_out(adder_output));

	//---  Instruction Memory module initialization
	inst_rom #(.INIT_PROGRAM({PATH, INSTR_NAME})) 
				 myInstrRom (.clock(clock), 
								 .reset(reset), 
								 .addr_in(toPCInput), 
								 
								 .data_out(instruction_out));
/**** END INSTRUCTION FETCH PHASE */
//--- PIPE ONE
if_id_regs if_id_regs(.clk(clock), .reset(reset), .w_e(1),
                      .inst_in(instruction_out),
							 .pc_in(toPCInput),
							 
							 .inst_out(if_id_inst),
							 .pc_out(if_id_pc));
							 
hazard_detection_unit Haz_D_U(.if_id_opcode(if_id_inst[31:26]),
                              .if_id_func(if_id_inst[5:0]),
										
										.if_id_w_e(if_id_w_e),
										.if_id_pc_w_e(if_id_pc_w_e));
/*** BEGIN INSTRUCTION DECODE PHASE */
//--- branch addition for the mux that will feed into the adder or JorB mux
Adder #(.WIDTH(W)) myBranchAdder(.a_in(if_id_pc),
										   .b_in(sign_extnd_imm << 2),
										 
										   .sum_out(toJumpMuxB) //--- wire for the branch location into the mux
										   );

wire [W-1:0] realJumpAddr; //--- the correct jump address to jump to from the mux
wire [27:0] shiftedInstr; //--- shifting IM[25:0] << 2 for the jumping ops
assign shiftedInstr = (if_id_inst[25:0] << 2);
//--- 4xMux for the 4 jump instructions to be put into the jump-branch-mux
Mux4 #(.W(W)) myJumpAdderMux(.A_in({if_id_pc[31:28], shiftedInstr}),
                             .B_in({if_id_pc[31:28], shiftedInstr}),
									  .C_in(read_data_one),
									  .D_in(read_data_one),
									  .sel_in(pc_jumper),
									  
									  .V_out(realJumpAddr));
									 
//--- Mux for the branch or the jump instructions
Mux2 #(.W(W)) myJumpMux(.A_in(toJumpMuxB), 
                        .B_in(realJumpAddr), 
								.sel_in(Jump), 
								
								.V_out(pcJorB));
					 
//--- MUX for the Register File inputs
/*
Mux2 #(.W(5)) myRegDst (.A_in(instruction_out[20:16]), 
                        .B_in(instruction_out[15:11]), 
								.sel_in(RegDst),
								.V_out(write_reg_from_mux));
*/
Mux4 #(.W(5)) myRegDst (.A_in(if_id_inst[20:16]), 
                        .B_in(if_id_inst[15:11]), 
								.C_in(5'd31),
								.D_in(32'b0),
								.sel_in(RegDst),
								
								.V_out(write_reg_from_mux));
								
//--- SIGN EXTENSION MODULE INITIALIZATION
SignExtender mySignExtender(.sixteen_bit_in(if_id_inst[15:0]), 

                            .thirtytwo_bit_out(sign_extnd_imm));
									 
//--- ZERO EXTENSION MODULE
zeroExtender myZeroExtender(.sixteen_bit_in(if_id_inst[15:0]),
									 
									 .thirtytwo_bit_out(zeroed_imm));
//--- SIGN EXTENSION MUX
wire logical_extension;
wire [W-1:0] processed_immediate;
Mux2 #(.W(W)) myExtensionMux(.A_in(sign_extnd_imm), 
                             .B_in(zeroed_imm), 
									  .sel_in(logical_extension), 
									  
									  .V_out(processed_immediate));

//--- REGISTER FILE MODULE INITIALIZATION
Registers myRegister(.read1_in(if_id_inst[25:21]), 
                     .read2_in(if_id_inst[20:16]), 
							.write_in(write_reg_from_mux), 
							.write_data_in(to_write_data),
							.data1_out(read_data_one),
							.data2_out(read_data_two),
							.write_en_in(RegWrite),
							.reset(reset),
							.clk(clock));

wire [W-1:0] B_data_ALU_mux;
//--- MUX for the ALU's B_in pin
Mux2 #(.W(32)) myALUSrc (.A_in(read_data_two), 
                         .B_in(processed_immediate),
								 .sel_in(ALUSrc),
								 
								 .V_out(B_data_ALU_mux));

wire [W-1:0] O_out;
//--- The ALU module initialization
alu myALU(.A_in(read_data_one), 
          .B_in(B_data_ALU_mux),
			 .Func_in(Funct_in), 
			 
			 
			 .Branch_out(Branch_out),
			 .Jump_out(Jump_out),
			 .O_out(O_out));

wire [W-1:0] dm_readdata_out;
//--- The Data Memory module Initialization
data_memory #(.INIT_PROGRAM0({PATH, RAM_NAME, "0.memh"}),
              .INIT_PROGRAM1({PATH, RAM_NAME, "1.memh"}),
              .INIT_PROGRAM2({PATH, RAM_NAME, "2.memh"}),
				  .INIT_PROGRAM3({PATH, RAM_NAME, "3.memh"}))
				myDataMemory (.writedata_in(read_data_two), 
				              .addr_in(O_out), 
								  .readdata_out(dm_readdata_out), 
								  .re_in(re_in),
								  .we_in(we_in),
								  .size_in(size_out),
								  .unsigned_trigger_in(unsigned_trigger_out), //--- custom trigger bit
								  
				              .serial_in(serial_in), 
								  .serial_valid_in(serial_valid_in), 
								  .serial_ready_in(serial_ready_in),
								  .serial_out(serial_out), 
								  .serial_rden_out(serial_rden_out), 
								  .serial_wren_out(serial_wren_out),
								  .clock(clock), 
								  .reset(reset));

//--- MUX for the write data into the Register File Write Register
/*
Mux2 #(.W(W)) myMemToReg (.A_in(O_out), 
                          .B_in(dm_readdata_out),
						    	  .sel_in(MemToReg),
								  
							     .V_out(to_write_data));
*/
Mux4 #(.W(W)) myMemToReg (.A_in(O_out),
								  .B_in(dm_readdata_out),
								  .C_in({if_id_inst[15:0], 16'b0}),
								  .D_in(adder_output),
								  .sel_in(MemToReg),
								  
								  .V_out(to_write_data));
								  
control_unit myControlUnit(.opcode_in(if_id_inst[31:26]), 
									.funct_in(if_id_inst[5:0]),
									.branch_LSB_in(if_id_inst[16]),
									
									.unsigned_trigger_out(unsigned_trigger_out),
									.logical_extension(logical_extension),
									.pc_jumper(pc_jumper),
									.size_out(size_out),
									.funct_in_out(Funct_in),
									.reg_dst_out(RegDst),
									.re_in_out(re_in),
									.mem_to_reg_out(MemToReg),
									.we_in_out(we_in),
									.alu_src_out(ALUSrc),
									.reg_write_out(RegWrite),
									.jump_out(Branch));
									
endmodule
