module hazard_detection_unit#(parameter W = 32)(
	input [5:0] if_id_opcode,
	input [5:0] if_id_func,
	
	output reg if_id_w_e,
	output reg if_id_pc_w_e
);

always @ (*) begin
  //if(if_id_opcode == 6'h2 || if_id_opcode == 6'h3 || if_id_opcode == 6'h4 ||
  //  	if_id_opcode == 6'h5 || (if_id_func == 6'h8 && if_id_opcode == 6'h0)) 
	//begin		
	//	if_id_w_e = 0;
	//	if_id_pc_w_e = 0;
	//end
	//else begin
		if_id_w_e = 1;
		if_id_pc_w_e = 1;
	//end
end
endmodule
