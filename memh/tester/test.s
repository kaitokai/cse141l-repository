#
#  CSE 141L Example Application
#  Prints "Hello World" to the serial interface at 0xffff0000
#  Avoids most MIPS instructions and all branches.
#  Will not work with SPIM because this does not check for buffer overflows on the serial interface (only the first character will print in SPIM)
#
#
#  Change Log:
#	1/18/2012 - Adrian Caulfield - Initial Implementation
#
#
#

.text

.globl	__start
#.type __start,@function

#	.set	noreorder
	__start:
	jal main
main:
	addi $20, $20, 0x0008
	lui $21, 0x1000
	sb $20, 0($21)
	lw $19, 0($21)
