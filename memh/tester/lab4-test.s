#
#  CSE 141L Example Application
#  Prints "Hello World" to the serial interface at 0xffff0000
#  Avoids most MIPS instructions and all branches.
#  Will not work with SPIM because this does not check for buffer overflows on the serial interface (only the first character will print in SPIM)
#
#
#  Change Log:
#	1/18/2012 - Adrian Caulfield - Initial Implementation
#
#
#

.text

.globl	__start
#.type __start,@function

#	.set	noreorder
	__start:
	jal main
printhello:
	addi $6, $5, 24
loop:
	lw	$10,0($5)		# $10 is now 'H'
	sw	$10,12($4)		# write word
	addi $5, $5, 4
	bne $5, $6, loop
	jr  $31
	
main:
	addi	$10,$0,0x4000
	sub	$20,$0,$10
	sub	$20,$20,$10		# $20 should now contain 0xffffc000
	sub	$20,$20,$10		# $20 should now contain 0xffff8000
	sub	$20,$20,$10		# $20 should now contain 0xffff0000

	addi	$11,$0,0x4000
	add	$11,$11,$11		#0x08000
	add	$11,$11,$11		#0x10000
	add	$11,$11,$11		#0x20000
	add	$11,$11,$11		#0x40000
	add	$11,$11,$11		#0x80000
	add	$11,$11,$11		#0x100000
	add	$11,$11,$11		#0x200000
	add	$11,$11,$11		#0x400000
	add	$11,$11,$11		#0x800000
	add	$11,$11,$11		#0x1000000
	add	$11,$11,$11		#0x2000000
	add	$11,$11,$11		#0x4000000
	add	$11,$11,$11		#0x8000000
	add	$11,$11,$11		#0x10000000

	addi	$10,$0,0x48		# $10 is now 'H'
	sw	$10,0($11)		# store word
	add $0, $0, $0

	addi	$10,$0,0x65		# 'e'
	sw	$10,4($11)		# store word
	add $0, $0, $0

	addi	$10,$0,0x6C		# 'l'
	sw	$10,8($11)		# store word
	add $0, $0, $0

	sw	$10,12($11)		# store word
	add $0, $0, $0

	addi	$10,$0,0x6F		# 'o'
	sw	$10,16($11)		# store word
	add $0, $0, $0

	addi	$10,$0,0x20		# ' '
	sw	$10,20($11)		# store word
	add $0, $0, $0
	add	$4, $20, $0
	add	$5, $11, $0
	jal printhello
	addi	$10,$0,0x57		# 'W'
	sw	$10,12($20)		# write word
	add $0, $0, $0

	addi	$10,$0,0x6F		# 'o'
	sw	$10,12($20)		# write word
	add $0, $0, $0

	addi	$10,$0,0x72		# 'r'
	sw	$10,12($20)		# write word
	add $0, $0, $0

	addi	$10,$0,0x6C		# 'l'
	sw	$10,12($20)		# write word
	add $0, $0, $0

	addi	$10,$0,0x64		# 'd'
	sw	$10,12($20)		# write word
	add $0, $0, $0

