`timescale 1ns / 1ns

module Mux4#(parameter W = 16)
   (
    input      [W-1:0]  A_in,
    input      [W-1:0]  B_in,
    input      [W-1:0]  C_in,
    input      [W-1:0]  D_in,
    
    output reg [W-1:0]  V_out,
    
    input      [1:0]    sel_in
    );
   
   always@(*)
     begin
	if (sel_in == 2'b00)
	  V_out = A_in;
	else if (sel_in == 2'b01)
	  V_out = B_in;
	else if (sel_in == 2'b10)
	  V_out = C_in;
	else 
	  V_out = D_in;
     end
endmodule