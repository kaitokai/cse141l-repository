`timescale 1ns/1ps
module test_registers();
reg clk;
reg reset;
reg [4:0] write_in = 0;
reg [4:0] read1_in = 1;
reg [4:0] read2_in = 2;
//reg [31:0] write_data_in = 32'h0000_0005;
reg write_en_in = 1;

wire [31:0] write_data_in;
wire [31:0] data1_out;
wire [31:0] data2_out;

Registers register(.write_in(write_in),
                   .read1_in(read1_in),
						 .read2_in(read2_in),
						 .write_data_in(write_data_in),
						 .write_en_in(write_en_in),
						 .clk(clk),
						 .reset(reset),
						 
						 .data1_out(data1_out),
						 .data2_out(data2_out)
						 );

reg [31:0] a_in = 0;
reg [31:0] b_in = 0;
Adder #(32) foo(.a_in(a_in), .b_in(b_in), .sum_out(write_data_in));

initial begin
   clk = 0;
	forever #10 clk = ~clk;
end

initial begin
   reset = 1;
	@(negedge clk);
	reset = 0;
	a_in = 1;
	@(negedge clk);
	$display("Write Register: %d", write_in);
	write_in = 1;
	read1_in = 0;
	@(negedge clk);
	$display("Last register's value now: %d\n", data1_out);
	$display("Write Register: %d", write_in);
	write_in = 0;
	read1_in = 1;
	a_in = 5;
	@(negedge clk);
	$display("Last register's value now: %d\n", data1_out);
	$display("Write Register: %d", write_in);
	write_in = 1;
	read1_in = 1;
	@(negedge clk);
	$display("Last register's value now: %d\n", data1_out);
	$display("Write Register: %d", write_in);
	write_in = 1;
	read1_in = 0;
	b_in = 5;
	@(negedge clk);
	write_in = 2;
	
end

endmodule 
