library verilog;
use verilog.vl_types.all;
entity SignExtender is
    generic(
        WIDTH           : integer := 16
    );
    port(
        sixteen_bit_in  : in     vl_logic_vector;
        thirtytwo_bit_out: out    vl_logic_vector
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of WIDTH : constant is 1;
end SignExtender;
