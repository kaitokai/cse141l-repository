library verilog;
use verilog.vl_types.all;
entity processor is
    generic(
        W               : integer := 8
    );
    port(
        clock           : in     vl_logic;
        reset           : in     vl_logic;
        serial_in       : in     vl_logic_vector(7 downto 0);
        serial_valid_in : in     vl_logic;
        serial_ready_in : in     vl_logic;
        serial_out      : out    vl_logic_vector(7 downto 0);
        serial_rden_out : out    vl_logic;
        serial_wren_out : out    vl_logic
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of W : constant is 1;
end processor;
