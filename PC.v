/*
 *  Written by CSE141L peeps
 */
module PC#(parameter DEFAULT_START_VALUE = 32'h003F_FFFC, BITSIZE = 32)(
	// DRIVEN INPUTS
	input clock,
	input reset,
	input pc_w_e,
	
	// INPUT
	input [BITSIZE-1:0] locat_in,
	
	// OUTPUT
	output reg [BITSIZE-1:0] addr_out
);
	
	always @ (posedge clock) begin
	   if (~reset) begin // prevent swapping during clock cycle
   		addr_out <= locat_in;
		end
		else if(pc_w_e) begin
		   addr_out <= DEFAULT_START_VALUE;
		end
	end
endmodule //PC
