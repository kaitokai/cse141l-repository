module Adder#(parameter WIDTH = 8)(
	input [WIDTH-1:0] a_in, b_in,
	output reg [WIDTH-1:0] sum_out );
	
	always @ (*) begin
		sum_out = a_in + b_in;
	end
endmodule
