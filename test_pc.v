`timescale 1ns/1ps

module test_pc;
	reg clk;
	reg reset = 0;
	wire [31:0] outstuff;
	reg [31:0] locat = 64'h00400000;
	PC dut (.clock(clk), .reset(reset), .locat_in(locat), .addr_out(outstuff));
	initial begin
		clk = 0;
		forever #10 clk = !clk;
	end
	
	initial begin
		@(negedge clk);
		locat = dut.addr_out + 4;
		@(negedge clk);		
		locat = dut.addr_out + 4;
		@(negedge clk);
		locat = dut.addr_out + 4;
		@(negedge clk);
		locat = dut.addr_out + 4;
	end
	
	
endmodule
