`timescale 1ns / 1ns

module Mux2_test#(parameter W = 16);

// Regs for the inputs.  Wires for the outputs.
reg   [W-1:0] A;
reg   [W-1:0] B;
reg           sel;
wire  [W-1:0] V;

Mux2 dut (.A_in(A), .B_in(B), .sel_in(sel), .V_out(V));

// Since Mux2 is a combinational block (there's no clock), we just need one initial block
initial
  begin
	// the test vector here is pretty simple.
	 #10 A = 10; B = 32; sel = 0;
	 #10 A = 10; B = 32; sel = 1;
  end
endmodule    