`timescale 1ns / 1ns

module Mux2#(parameter W = 16)
   (
    input      [W-1:0]  A_in,
    input      [W-1:0]  B_in,
    
    output reg [W-1:0]  V_out,
    
    input               sel_in
    );
   
   always@(*)
		  begin
		if (sel_in == 1'b0)
		  V_out = A_in;
		else 
		  V_out = B_in;
   end
endmodule