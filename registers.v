module Registers#(parameter W = 32)(
    input [4:0] read1_in,
	 input [4:0] read2_in,
	 input [4:0] write_in,
	 input [W-1:0] write_data_in,
	 input write_en_in,
	 input reset,
	 input clk,
	 
	 output reg [W-1:0] data1_out,
	 output reg [W-1:0] data2_out
);
localparam REGISTER_COUNT=32;

reg [W-1:0] registers [REGISTER_COUNT-1:0];
reg [31:0] i;

always @ (posedge clk) begin
	if (reset) begin
		for (i = 0; i < REGISTER_COUNT; i = i + 1) begin
			registers[i] <= 0;
		end
	end
	else begin
		if ((write_en_in) && (write_in > 0)) begin // don't write to register 0
			registers[write_in] <= write_data_in;
		end
	end
end

// read only operations
always @ (*) begin	
	begin
		data1_out = registers[read1_in];
		data2_out = registers[read2_in];
	end
end

endmodule
