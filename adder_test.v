`timescale 1ns / 1ps

module adder_test;
reg clk;
reg [31:0] a = 1;
reg [31:0] b = 2;
wire [32:0] out;
Adder #(32) dut (.a_in(a), .b_in(b), .sum_out(out));

//initial begin
//	clk = 0;
//	forever #10 clk = !clk;
//end

//initial begin
//	$display(out);
//end

endmodule