module if_id_regs#(parameter W = 32)(
    input clk,
	 input reset,
	 input w_e,
	 
	 input [W-1:0] inst_in,
	 input [W-1:0] pc_in, //--- pc+4
	 
	 output reg [W-1:0] inst_out,
	 output reg [W-1:0] pc_out
);

    always @ (posedge clk) begin
	    if(reset) begin
			inst_out <= 0;
			pc_out <= 0;
		 end
/*		 else if(w_e) begin
		   inst_out <= inst_in;
			pc_out <= pc_in;
		 end
*/		 else begin
			inst_out <= inst_in;
			pc_out <= pc_in;
		 end
		 //--- if w_e isn't triggered we simply do not write anything to the pipe
	 end
endmodule

module id_ex_regs#(parameter W = 32)(
);
endmodule